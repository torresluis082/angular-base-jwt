import {Component, OnInit} from '@angular/core';
import {FormBuilder} from '@angular/forms';
import {Router} from '@angular/router';
import {ProductoService} from '../service/Producto.service';
import {ProductoRequest} from '../model/ProductoRequest';

@Component({
  selector: 'app-lista',
  templateUrl: './lista.component.html',
  styleUrls: ['./lista.component.scss']
})
export class ListaComponent implements OnInit {

  listas: ProductoRequest[];

  constructor(
    private router: Router, private prod: ProductoService) {
  }

  ngOnInit(): void {
    this.prod.getlista()
      .subscribe(x => {
        this.listas = x;
      });
  }

}
