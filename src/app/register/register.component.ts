import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {Router} from '@angular/router';
import {LoginService} from '../service/Login.service';
import {LoginRequest} from '../model/LoginRequest';
import {ProductoRequest} from '../model/ProductoRequest';
import {ProductoService} from '../service/Producto.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  formRegistro = new FormGroup({});

  constructor(private fb: FormBuilder,
              private router: Router, private prod: ProductoService) {
  }

  ngOnInit(): void {
    this.formRegistro = this.fb.group({
      code: new FormControl(),
      name: new FormControl(),
      tipe: new FormControl(),
      quantity: new FormControl(),
    });
  }

  save() {
    let prod = new ProductoRequest(
      this.formRegistro.controls.code.value,
      this.formRegistro.controls.name.value,
      this.formRegistro.controls.tipe.value,
      this.formRegistro.controls.quantity.value
    );
    this.prod.getProcedimientos(prod)
      .subscribe(x => {
        this.router.navigate(['/lista']);
      });
  }

}
