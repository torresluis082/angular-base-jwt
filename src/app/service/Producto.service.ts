import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {ProductoRequest} from '../model/ProductoRequest';
import {LoginService} from './Login.service';

@Injectable({
  providedIn: 'root'
})
export class ProductoService {

  constructor(private http: HttpClient) {
  }


  getProcedimientos(prod: ProductoRequest): Observable<ProductoRequest> {
    let headers: HttpHeaders = new HttpHeaders();
    headers = headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');

    return this.http.post<ProductoRequest>(`http://localhost:8081/registro`, prod)
      .pipe(
        map(response => {
          return new ProductoRequest(response.code,
            response.name,
            response.tipe,
            response.quantity);
        })
      );
  }

  getlista(): Observable<ProductoRequest[]> {
    const auth_token = LoginService.token;

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: `Bearer ${auth_token}`
      })
    };

    return this.http.post<ProductoRequest[]>(`http://localhost:8081/lista`, httpOptions)
      .pipe(
        map(response => {
          return response;
        })
      );
  }
}
