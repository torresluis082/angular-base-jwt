import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {TokenResponse} from '../model/TokenResponse';
import {LoginRequest} from '../model/LoginRequest';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  static token = '';

  constructor(private http: HttpClient) {
  }

  getLogin(login: LoginRequest): Observable<TokenResponse> {

    return this.http.post<TokenResponse>(`http://localhost:8081/authenticate`, login)
      .pipe(
        map(response => {
          LoginService.token = response.token;
          return new TokenResponse(response.token);
        })
      );
  }

}
