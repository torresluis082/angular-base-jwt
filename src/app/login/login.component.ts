import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {LoginService} from '../service/Login.service';
import {LoginRequest} from '../model/LoginRequest';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  formValidation = new FormGroup({});

  constructor(private fb: FormBuilder,
              private router: Router, private loginService: LoginService) {
  }

  ngOnInit(): void {
    this.formValidation = this.fb.group({
      user: new FormControl(),
      pwd: new FormControl(),
    });
  }

  // tslint:disable-next-line:typedef
  login() {
    // tslint:disable-next-line:max-line-length
    this.loginService.getLogin(
      new LoginRequest(this.formValidation.controls.user.value, this.formValidation.controls.pwd.value))
      .subscribe(x => {
        if (x.token !== null && x.token !== '') {
          this.router.navigate(['/lista']);
        } else {
          console.log('Credenciales invalidas');
        }
      });
  }

}
